# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.1](https://gitlab.com/cptpackrat/stagehand/compare/v1.2.0...v1.2.1) (2022-07-26)


### Bug Fixes

* Fixed emptyDir/emptyDirSync failing on broken symlinks. ([5d73321](https://gitlab.com/cptpackrat/stagehand/commit/5d733213962eb3979effa9f8f5fffc8058924b17))

## [1.2.0](https://gitlab.com/cptpackrat/stagehand/compare/v1.1.0...v1.2.0) (2022-07-24)


### Features

* Added config file and variable hashing via hashValue. ([4b35600](https://gitlab.com/cptpackrat/stagehand/commit/4b35600c712a1a79b0f5b7382bdf6301f4eeee45))
* Added custom inspect rendering to match patterns. ([815a3e6](https://gitlab.com/cptpackrat/stagehand/commit/815a3e60cae6dd4752a8d63dcbd1b95a182103b7))


### Bug Fixes

* Fixed hashDir/hashDirSync not distinguishing between files and symlinks. ([288ac29](https://gitlab.com/cptpackrat/stagehand/commit/288ac294a14cb12e4b87cf97b6f149ff62efb25d))
* Fixed hashDir/hashDirSync using locale-dependent sorting. ([aef5f12](https://gitlab.com/cptpackrat/stagehand/commit/aef5f12dbbc192553ba29ca6cfc4c5c7c5d467c8))


### Housekeeping

* Updated dependencies. ([109842d](https://gitlab.com/cptpackrat/stagehand/commit/109842da1b35cd05dc873464911cbf6a78d79430))

## [1.1.0](https://gitlab.com/cptpackrat/stagehand/compare/v1.0.0...v1.1.0) (2022-06-22)


### Features

* Added exclusion support to match patterns. ([576bcd1](https://gitlab.com/cptpackrat/stagehand/commit/576bcd16005dbaacfd1d495cf3f37f715a628c64))


### Housekeeping

* Updated dependencies and copyright. ([465a35c](https://gitlab.com/cptpackrat/stagehand/commit/465a35c7ebb486905c17386201ab2fd7db765f77))

## 1.0.0 (2021-06-17)

* Initial release.
