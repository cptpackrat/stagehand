# @nfi/stagehand
[![npm version][npm-image]][npm-url]
[![pipeline status][pipeline-image]][pipeline-url]
[![coverage status][coverage-image]][coverage-url]
[![standard-js][standard-image]][standard-url]

A set of utilities for managing staging directories and build artifacts.

## Installation
```
npm install @nfi/stagehand
```

## Documentation
API documentation is available [here](https://cptpackrat.gitlab.io/stagehand).

[npm-image]: https://img.shields.io/npm/v/@nfi/stagehand.svg
[npm-url]: https://www.npmjs.com/package/@nfi/stagehand
[pipeline-image]: https://gitlab.com/cptpackrat/stagehand/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/cptpackrat/stagehand/commits/master
[coverage-image]: https://gitlab.com/cptpackrat/stagehand/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/cptpackrat/stagehand/commits/master
[standard-image]: https://img.shields.io/badge/code%20style-standard-brightgreen.svg
[standard-url]: http://standardjs.com/
