import { inspect } from 'util'
import { appendFileSync } from 'fs'
import {
  makeTestRoot,
  readTree,
  treeDir,
  treeFile,
  treeLink
} from '@nfi/testtree'
import {
  copyDir,
  copyDirSync,
  copyFile,
  copyFileSync,
  copyLink,
  copyLinkSync,
  copyMatches,
  copyMatchesSync,
  emptyDir,
  emptyDirSync,
  hashDir,
  hashDirSync,
  hashFile,
  hashFileSync,
  hashLink,
  hashLinkSync,
  hashValue,
  linkDir,
  linkDirSync,
  linkFile,
  linkFileSync,
  linkMatches,
  linkMatchesSync,
  matchDir,
  matchDirSync,
  Matched,
  Pattern,
  walkDir,
  walkDirSync,
  Walked
} from '../src'

const {
  makeTestDir,
  makeTestTree,
  wipe
} = makeTestRoot()

afterAll(() => wipe())

describe.each([
  ['copyFile', copyFile],
  ['copyFileSync', copyFileSync]
])('%s', (_, fn) => {
  it('copies from src to dst', async () => {
    const { path, join } = makeTestTree({
      src: treeFile('testing')
    })
    await fn(join('src'), join('dst'))
    expect(readTree(path)).toStrictEqual({
      src: treeFile('testing'),
      dst: treeFile('testing')
    })
    appendFileSync(join('src'), 'testing')
    expect(readTree(path)).toStrictEqual({
      src: treeFile('testingtesting'),
      dst: treeFile('testing')
    })
  })
  it('rejects missing src', async () => {
    const { join } = makeTestDir()
    await expect(async () => {
      await fn(join('src'), join('dst'))
    }).rejects.toThrow('ENOENT')
  })
  it('rejects existing dst', async () => {
    const { join } = makeTestTree({
      src: treeFile('testing'),
      dst: treeFile('testing')
    })
    await expect(async () => {
      await fn(join('src'), join('dst'))
    }).rejects.toThrow('EEXIST')
  })
})

describe.each([
  ['copyLink', copyLink],
  ['copyLinkSync', copyLinkSync]
])('%s', (_, fn) => {
  it('copies from src to dst', async () => {
    const { path, join } = makeTestTree({
      src: treeLink('testing')
    })
    await fn(join('src'), join('dst'))
    expect(readTree(path)).toStrictEqual({
      src: treeLink('testing'),
      dst: treeLink('testing')
    })
  })
  it('rejects missing src', async () => {
    const { join } = makeTestDir()
    await expect(async () => {
      await fn(join('src'), join('dst'))
    }).rejects.toThrow('ENOENT')
  })
  it('rejects existing dst', async () => {
    const { join } = makeTestTree({
      src: treeLink('testing'),
      dst: treeLink('testing')
    })
    await expect(async () => {
      await fn(join('src'), join('dst'))
    }).rejects.toThrow('EEXIST')
  })
})

describe.each([
  ['copyDir', copyDir],
  ['copyDirSync', copyDirSync]
])('%s', (_, fn) => {
  it('copies from src to dst', async () => {
    const tree = {
      foo: treeDir({ boo: treeLink('testing') }),
      bar: treeDir({ baz: treeFile('testing') })
    }
    const { path, join } = makeTestTree({
      src: treeDir(tree)
    })
    await fn(join('src'), join('dst'))
    expect(readTree(path)).toStrictEqual({
      src: treeDir(tree),
      dst: treeDir(tree)
    })
    appendFileSync(join('src', 'bar', 'baz'), 'testing')
    expect(readTree(path)).toStrictEqual({
      src: treeDir({
        ...tree,
        bar: treeDir({ baz: treeFile('testingtesting') })
      }),
      dst: treeDir(tree)
    })
  })
  it('rejects missing src', async () => {
    const { join } = makeTestDir()
    await expect(async () => {
      await fn(join('src'), join('dst'))
    }).rejects.toThrow('ENOENT')
  })
  it('accepts existing dst', async () => {
    const { join } = makeTestTree({
      src: treeDir(),
      dst: treeDir()
    })
    await fn(join('src'), join('dst'))
  })
  it('accepts existing files in dst', async () => {
    const { join } = makeTestTree({
      src: treeDir({ foo: treeFile('testing') }),
      dst: treeDir({ bar: treeFile('testing') })
    })
    await fn(join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeFile('testing'),
      bar: treeFile('testing')
    })
  })
  it('accepts existing links in dst', async () => {
    const { join } = makeTestTree({
      src: treeDir({ foo: treeLink('testing') }),
      dst: treeDir({ bar: treeLink('testing') })
    })
    await fn(join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeLink('testing'),
      bar: treeLink('testing')
    })
  })
  it('accepts existing dirs in dst', async () => {
    const { join } = makeTestTree({
      src: treeDir({ foo: treeDir() }),
      dst: treeDir({ bar: treeDir() })
    })
    await fn(join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeDir(),
      bar: treeDir()
    })
  })
  it('rejects overlapping files in dst', async () => {
    const { join } = makeTestTree({
      src: treeDir({ foo: treeFile('testing') }),
      dst: treeDir({ foo: treeFile('testing') })
    })
    await expect(async () => {
      await fn(join('src'), join('dst'))
    }).rejects.toThrow('EEXIST')
  })
  it('rejects overlapping links in dst', async () => {
    const { join } = makeTestTree({
      src: treeDir({ foo: treeLink('testing') }),
      dst: treeDir({ foo: treeLink('testing') })
    })
    await expect(async () => {
      await fn(join('src'), join('dst'))
    }).rejects.toThrow('EEXIST')
  })
  it('accepts overlapping dirs in dst', async () => {
    const { join } = makeTestTree({
      src: treeDir({
        foo: treeDir({ bar: treeFile('testing') })
      }),
      dst: treeDir({
        foo: treeDir({ boo: treeFile('testing') })
      })
    })
    await fn(join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeDir({
        bar: treeFile('testing'),
        boo: treeFile('testing')
      })
    })
  })
})

describe.each([
  ['linkFile', linkFile],
  ['linkFileSync', linkFileSync]
])('%s', (_, fn) => {
  it('links from src to dst', async () => {
    const { path, join } = makeTestTree({
      src: treeFile('testing')
    })
    await fn(join('src'), join('dst'))
    expect(readTree(path)).toStrictEqual({
      src: treeFile('testing'),
      dst: treeFile('testing')
    })
    appendFileSync(join('src'), 'testing')
    expect(readTree(path)).toStrictEqual({
      src: treeFile('testingtesting'),
      dst: treeFile('testingtesting')
    })
  })
  it('rejects missing src', async () => {
    const { join } = makeTestDir()
    await expect(async () => {
      await fn(join('src'), join('dst'))
    }).rejects.toThrow('ENOENT')
  })
  it('rejects existing dst', async () => {
    const { join } = makeTestTree({
      src: treeFile('testing'),
      dst: treeFile('testing')
    })
    await expect(async () => {
      await fn(join('src'), join('dst'))
    }).rejects.toThrow('EEXIST')
  })
})

describe.each([
  ['linkDir', linkDir],
  ['linkDirSync', linkDirSync]
])('%s', (_, fn) => {
  it('links from src to dst', async () => {
    const tree = {
      foo: treeDir({ boo: treeLink('testing') }),
      bar: treeDir({ baz: treeFile('testing') })
    }
    const { path, join } = makeTestTree({
      src: treeDir(tree)
    })
    await fn(join('src'), join('dst'))
    expect(readTree(path)).toStrictEqual({
      src: treeDir(tree),
      dst: treeDir(tree)
    })
    appendFileSync(join('src', 'bar', 'baz'), 'testing')
    expect(readTree(path)).toStrictEqual({
      src: treeDir({
        ...tree,
        bar: treeDir({ baz: treeFile('testingtesting') })
      }),
      dst: treeDir({
        ...tree,
        bar: treeDir({ baz: treeFile('testingtesting') })
      })
    })
  })
  it('rejects missing src', async () => {
    const { join } = makeTestDir()
    await expect(async () => {
      await fn(join('src'), join('dst'))
    }).rejects.toThrow('ENOENT')
  })
  it('accepts existing dst', async () => {
    const { join } = makeTestTree({
      src: treeDir(),
      dst: treeDir()
    })
    await fn(join('src'), join('dst'))
  })
  it('accepts existing files in dst', async () => {
    const { join } = makeTestTree({
      src: treeDir({ foo: treeFile('testing') }),
      dst: treeDir({ bar: treeFile('testing') })
    })
    await fn(join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeFile('testing'),
      bar: treeFile('testing')
    })
  })
  it('accepts existing links in dst', async () => {
    const { join } = makeTestTree({
      src: treeDir({ foo: treeLink('testing') }),
      dst: treeDir({ bar: treeLink('testing') })
    })
    await fn(join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeLink('testing'),
      bar: treeLink('testing')
    })
  })
  it('accepts existing dirs in dst', async () => {
    const { join } = makeTestTree({
      src: treeDir({ foo: treeDir() }),
      dst: treeDir({ bar: treeDir() })
    })
    await fn(join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeDir(),
      bar: treeDir()
    })
  })
  it('rejects overlapping files in dst', async () => {
    const { join } = makeTestTree({
      src: treeDir({ foo: treeFile('testing') }),
      dst: treeDir({ foo: treeFile('testing') })
    })
    await expect(async () => {
      await fn(join('src'), join('dst'))
    }).rejects.toThrow('EEXIST')
  })
  it('rejects overlapping links in dst', async () => {
    const { join } = makeTestTree({
      src: treeDir({ foo: treeLink('testing') }),
      dst: treeDir({ foo: treeLink('testing') })
    })
    await expect(async () => {
      await fn(join('src'), join('dst'))
    }).rejects.toThrow('EEXIST')
  })
  it('accepts overlapping dirs in dst', async () => {
    const { join } = makeTestTree({
      src: treeDir({
        foo: treeDir({ bar: treeFile('testing') })
      }),
      dst: treeDir({
        foo: treeDir({ boo: treeFile('testing') })
      })
    })
    await fn(join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeDir({
        bar: treeFile('testing'),
        boo: treeFile('testing')
      })
    })
  })
})

describe.each([
  ['emptyDir', emptyDir],
  ['emptyDirSync', emptyDirSync]
])('%s', (_, fn) => {
  it('empties directories', async () => {
    const { path } = makeTestTree({
      foo: treeDir({ boo: treeLink('testing') }),
      bar: treeDir({ baz: treeFile('testing') })
    })
    await fn(path)
    expect(readTree(path)).toStrictEqual({})
  })
  it('creates missing directories', async () => {
    const { path, join } = makeTestDir()
    await fn(join('foo'))
    expect(readTree(path)).toStrictEqual({
      foo: treeDir()
    })
  })
})

describe('hashValue', () => {
  it('accepts primitive-like values', () => {
    expectUniqueHashes(7, [
      hashValue('foo'),
      hashValue('foo'),
      hashValue(12345),
      hashValue(12345),
      hashValue(12345n),
      hashValue(12345n),
      hashValue(true),
      hashValue(true),
      hashValue(false),
      hashValue(false),
      hashValue(null),
      hashValue(null),
      hashValue(undefined),
      hashValue(undefined)
    ])
  })
  it('accepts plain arrays', () => {
    expectUniqueHashes(6, [
      hashValue([]),
      hashValue([]),
      hashValue([1, 2, 3]),
      hashValue([1, 2, 3]),
      hashValue(['1', '2', '3']),
      hashValue(['1', '2', '3']),
      hashValue([[1], [2], [3]]),
      hashValue([[1], [2], [3]]),
      hashValue([['1'], ['2'], ['3']]),
      hashValue([['1'], ['2'], ['3']]),
      hashValue([true, false, null, undefined]),
      hashValue([true, false, null, undefined])
    ])
  })
  it('accepts plain objects', () => {
    expectUniqueHashes(6, [
      hashValue({}),
      hashValue({}),
      hashValue({ a: 1, b: 2, c: 3 }),
      hashValue({ a: 1, b: 2, c: 3 }),
      hashValue({ a: '1', b: '2', c: '3' }),
      hashValue({ a: '1', b: '2', c: '3' }),
      hashValue({ a: { b: 2, c: 3, d: 4 } }),
      hashValue({ a: { b: 2, c: 3, d: 4 } }),
      hashValue({ a: { b: '2', c: '3', d: '4' } }),
      hashValue({ a: { b: '2', c: '3', d: '4' } }),
      hashValue({ a: true, b: false, c: null, d: undefined }),
      hashValue({ a: true, b: false, c: null, d: undefined })
    ])
  })
  it('accepts plain buffers', () => {
    expectUniqueHashes(3, [
      hashValue(Buffer.alloc(0)),
      hashValue(Buffer.from('foo')),
      hashValue(Buffer.from('foo')),
      hashValue(Buffer.from('bar')),
      hashValue(Buffer.from('bar'))
    ])
  })
  it('accepts plain dates', () => {
    expectUniqueHashes(3, [
      hashValue(new Date()),
      hashValue(new Date('1970-01-01')),
      hashValue(new Date('1970-01-01')),
      hashValue(new Date('2022-01-01')),
      hashValue(new Date('2022-01-01'))
    ])
  })
  it('accepts plain maps', () => {
    expectUniqueHashes(3, [
      hashValue(new Map()),
      hashValue(new Map()),
      hashValue(new Map([['a', 1], ['b', 2], ['c', 3]])),
      hashValue(new Map([['a', 1], ['b', 2], ['c', 3]])),
      hashValue(new Map([['a', '1'], ['b', '2'], ['c', '3']])),
      hashValue(new Map([['a', '1'], ['b', '2'], ['c', '3']]))
    ])
  })
  it('accepts plain regexes', () => {
    expectUniqueHashes(3, [
      hashValue(/foo/),
      hashValue(/foo/),
      hashValue(/ba+r/),
      hashValue(/ba+r/),
      /* eslint-disable-next-line prefer-regex-literals */
      hashValue(new RegExp('')),
      /* eslint-disable-next-line prefer-regex-literals */
      hashValue(new RegExp('foo')),
      /* eslint-disable-next-line prefer-regex-literals */
      hashValue(new RegExp('ba+r'))
    ])
  })
  it('accepts plain sets', () => {
    expectUniqueHashes(3, [
      hashValue(new Set()),
      hashValue(new Set()),
      hashValue(new Set([1, 2, 3])),
      hashValue(new Set([1, 2, 3])),
      hashValue(new Set(['1', '2', '3'])),
      hashValue(new Set(['1', '2', '3']))
    ])
  })
  it('accepts customised objects', () => {
    let count = 0
    class Custom implements hashValue.Custom {
      constructor (readonly value: any) {}
      [hashValue.custom] (hash: (value: any) => void): void {
        count++
        hash(this.value)
      }
    }
    expectUniqueHashes(3, [
      hashValue(new Custom('foo')),
      hashValue(new Custom('foo')),
      hashValue(new Custom('bar')),
      hashValue(new Custom('bar')),
      hashValue(new Custom('boo')),
      hashValue(new Custom('boo'))
    ])
    expect(count).toEqual(6)
  })
  it('rejects symbols', () => {
    expect(() => hashValue(Symbol('foo'))).toThrow('cannot hash symbol')
  })
  it('rejects functions', () => {
    expect(() => hashValue(() => {})).toThrow('cannot hash function')
  })
  it('rejects derived arrays', () => {
    class ExtendedArray extends Array<any> {}
    expect(() => hashValue(new ExtendedArray())).toThrow('cannot hash ExtendedArray')
  })
  it('rejects derived objects', () => {
    /* eslint-disable-next-line @typescript-eslint/no-extraneous-class */
    class ExtendedObject {}
    expect(() => hashValue(new ExtendedObject())).toThrow('cannot hash ExtendedObject')
  })
  it('rejects derived built-ins', () => {
    class ExtendedDate extends Date {}
    class ExtendedMap extends Map<any, any> {}
    class ExtendedRegExp extends RegExp {}
    class ExtendedSet extends Set<any> {}
    expect(() => hashValue(new ExtendedDate())).toThrow('cannot hash ExtendedDate')
    expect(() => hashValue(new ExtendedMap())).toThrow('cannot hash ExtendedMap')
    expect(() => hashValue(new ExtendedRegExp('foo'))).toThrow('cannot hash ExtendedRegExp')
    expect(() => hashValue(new ExtendedSet())).toThrow('cannot hash ExtendedSet')
  })
  it('rejects cycles within objects', () => {
    const foo = {}
    const bar = { foo }
    expect(() => hashValue(Object.assign(foo, { bar }))).toThrow('cyclic object')
  })
  it('rejects cycles within customised objects', () => {
    class Custom implements hashValue.Custom {
      constructor (readonly value: any) {}
      [hashValue.custom] (hash: (value: any) => void): void {
        hash(this.value)
      }
    }
    const foo = {}
    const bar = new Custom(foo)
    expect(() => hashValue(Object.assign(foo, { bar }))).toThrow('cyclic object')
  })
  it('rejects unknown algorithm', () => {
    expect(() => hashValue('foo', 'whatever')).toThrow('not supported')
  })
  it('ignores iteration order in objects', () => {
    expectUniqueHashes(1, [
      hashValue({ a: 'a', b: 'b', c: 'c', d: 'd' }),
      hashValue({ d: 'd', a: 'a', b: 'b', c: 'c' })
    ])
  })
  it('ignores iteration order in maps', () => {
    expectUniqueHashes(1, [
      hashValue(new Map([['a', 'a'], ['b', 'b'], ['c', 'c'], ['d', 'd']])),
      hashValue(new Map([['d', 'd'], ['a', 'a'], ['b', 'b'], ['c', 'c']]))
    ])
  })
  it('ignores iteration order in sets', () => {
    expectUniqueHashes(1, [
      hashValue(new Set(['a', 'b', 'c', 'd'])),
      hashValue(new Set(['d', 'a', 'b', 'c']))
    ])
  })
  it('ignores undefined properties in objects', () => {
    const deleted: Record<string, string> = { a: 'a', b: 'b', c: 'c', d: 'd' }
    /* eslint-disable-next-line @typescript-eslint/no-dynamic-delete */
    delete deleted.c
    expectUniqueHashes(1, [
      hashValue(deleted),
      hashValue({ a: 'a', b: 'b', d: 'd' }),
      hashValue({ a: 'a', b: 'b', c: undefined, d: 'd' })
    ])
  })
  it('ignores undefined elements in arrays', () => {
    const deleted: string[] = ['a', 'b', 'c', 'd']
    delete deleted[2]
    expectUniqueHashes(1, [
      hashValue(deleted),
      /* eslint-disable-next-line no-sparse-arrays */
      hashValue(['a', 'b', , 'd']),
      hashValue(['a', 'b', undefined, 'd'])
    ])
  })
  it('distinguishes between numbers and bigints', () => {
    expectUniqueHashes(4, [
      hashValue(0),
      hashValue(0n),
      hashValue(12345),
      hashValue(12345n)
    ])
  })
  it('distinguishes between strings and buffers', () => {
    expectUniqueHashes(4, [
      hashValue(''),
      hashValue('foo'),
      hashValue(Buffer.alloc(0)),
      hashValue(Buffer.from('foo')),
      hashValue(Buffer.from('foo').toString('utf8'))
    ])
  })
  it('distinguishes between strings and regexes', () => {
    expectUniqueHashes(4, [
      hashValue(''),
      hashValue('foo'),
      hashValue(/foo/),
      /* eslint-disable-next-line prefer-regex-literals */
      hashValue(new RegExp('')),
      /* eslint-disable-next-line prefer-regex-literals */
      hashValue(new RegExp('foo'))
    ])
  })
  it('distinguishes between strings and other primitive-like values', () => {
    expectUniqueHashes(12, [
      hashValue(12345),
      hashValue(12345n),
      hashValue(true),
      hashValue(false),
      hashValue(null),
      hashValue(undefined),
      hashValue('12345'),
      hashValue('12345n'),
      hashValue('true'),
      hashValue('false'),
      hashValue('null'),
      hashValue('undefined')
    ])
  })
  it('distinguishes between arrays, objects, maps and sets', () => {
    expectUniqueHashes(8, [
      hashValue([]),
      hashValue({}),
      hashValue(new Set()),
      hashValue(new Map()),
      hashValue(['a', 'b', 'c']),
      hashValue({ 0: 'a', 1: 'b', 2: 'c' }),
      hashValue(new Set([0, 'a', 1, 'b', 2, 'c'])),
      hashValue(new Map([[0, 'a'], [1, 'b'], [2, 'c']]))
    ])
  })
  it('distinguishes between nesting depths in arrays', () => {
    expectUniqueHashes(2, [
      hashValue([1, [2, 3, 4, 5], 6]),
      hashValue([1, [2, 3, 4], 5, 6])
    ])
  })
  it('distinguishes between nesting depths in objects', () => {
    expectUniqueHashes(2, [
      hashValue({ a: { b: 1, c: 2, d: 3 } }),
      hashValue({ a: { b: 1, c: 2 }, d: 3 })
    ])
  })
  it('distinguishes between values inside and outside of customised objects', () => {
    class Custom implements hashValue.Custom {
      constructor (readonly value: any) {}
      [hashValue.custom] (hash: (value: any) => void): void {
        hash(this.value)
      }
    }
    expectUniqueHashes(6, [
      hashValue('foo'),
      hashValue([1, 2, 3]),
      hashValue({ a: 1, b: 2 }),
      hashValue(new Custom('foo')),
      hashValue(new Custom([1, 2, 3])),
      hashValue(new Custom({ a: 1, b: 2 }))
    ])
  })
  it('returns expected values', () => {
    class Custom implements hashValue.Custom {
      constructor (readonly value: any) {}
      [hashValue.custom] (hash: (value: any) => void): void {
        hash(this.value)
      }
    }
    expect(hashValue('foo')).toEqual('6fd887bcf14cb28d72b6220892b9809e0e5d9cbb')
    expect(hashValue('bar')).toEqual('45e828da4a2e5364625a19b9f77381aa442b5b3d')
    expect(hashValue(123)).toEqual('00d515ee3915ae67985c49a68d91adf679b6d07c')
    expect(hashValue(123n)).toEqual('668fae6929cfa415f2cc0602d22d424c9e35f443')
    expect(hashValue(null)).toEqual('2be88ca4242c76e8253ac62474851065032d6833')
    expect(hashValue(true)).toEqual('281ba5365fc65c7ea47c3b306a292f9518c1cc70')
    expect(hashValue(false)).toEqual('73cd03305bbe68a1cbeb6bec28a9a52bfbbd09b4')
    expect(hashValue(undefined)).toEqual('d5d4cd07616a542891b7ec2d0257b3a24b69856e')
    expect(hashValue({})).toEqual('64979de8f20b4ff72ccbc9a30d9b8fd646424a14')
    expect(hashValue([1, 2, 3])).toEqual('83bfe963f0663d2a83a3eeb8c43bf4d2a01f4ff8')
    expect(hashValue({ a: 'a', b: 'b' })).toEqual('4025f8310b6d0673075ef0b24b48ea0d3665e937')
    expect(hashValue(/foo/)).toEqual('6e35e9e45254b362dbf983ac4880ae7bee691da0')
    expect(hashValue(Buffer.from('foo'))).toEqual('1b7aa75e24d649ad5016806a0ba8e18d7e401c61')
    expect(hashValue(new Custom('foo'))).toEqual('b54fee7e61e3463fdb6ea63999cfb587027b413f')
    expect(hashValue(new Date('1970-01-01'))).toEqual('fe57937ce2795484028d698da889185d7952e0e5')
    expect(hashValue(new Map())).toEqual('83df6248e8246871e68ea8e9eb704b3efb9e4633')
    expect(hashValue(new Set())).toEqual('5f00c0e19317ebcf8b4af4a9a5010271b2bb3b35')
  })
})

describe.each([
  ['hashFile', hashFile],
  ['hashFileSync', hashFileSync]
])('%s', (_, fn) => {
  const { join } = makeTestTree({
    foo: treeFile('testing'),
    bar: treeFile('testing'),
    boo: treeFile('testingtesting')
  })
  it('hashes files', async () => {
    const foo = await fn(join('foo'))
    const bar = await fn(join('bar'))
    const boo = await fn(join('boo'))
    expect(foo).toMatch(/^[a-f0-9]{40}$/)
    expect(bar).toMatch(/^[a-f0-9]{40}$/)
    expect(boo).toMatch(/^[a-f0-9]{40}$/)
    expect(foo).toEqual(bar)
    expect(foo).not.toEqual(boo)
  })
  it('accepts custom algorithm', async () => {
    const foo = await fn(join('foo'), 'sha256')
    const bar = await fn(join('bar'), 'sha256')
    const boo = await fn(join('boo'), 'sha256')
    expect(foo).toMatch(/^[a-f0-9]{64}$/)
    expect(bar).toMatch(/^[a-f0-9]{64}$/)
    expect(boo).toMatch(/^[a-f0-9]{64}$/)
    expect(foo).toEqual(bar)
    expect(foo).not.toEqual(boo)
  })
  it('rejects unknown algorithm', async () => {
    await expect(async () => {
      await fn(join('foo'), 'whatever')
    }).rejects.toThrow('not supported')
  })
  it('rejects missing file', async () => {
    await expect(async () => {
      await fn(join('baz'))
    }).rejects.toThrow('ENOENT')
  })
  it('returns expected values', async () => {
    expect(await fn(join('foo'))).toEqual('dc724af18fbdd4e59189f5fe768a5f8311527050')
    expect(await fn(join('bar'))).toEqual('dc724af18fbdd4e59189f5fe768a5f8311527050')
    expect(await fn(join('boo'))).toEqual('b0212be2cc6081fba3e0b6f3dc6e0109d6f7b4cb')
    expect(await fn(join('foo'), 'sha256')).toEqual('cf80cd8aed482d5d1527d7dc72fceff84e6326592848447d2dc0b0e87dfc9a90')
    expect(await fn(join('bar'), 'sha256')).toEqual('cf80cd8aed482d5d1527d7dc72fceff84e6326592848447d2dc0b0e87dfc9a90')
    expect(await fn(join('boo'), 'sha256')).toEqual('ae80976b013f1a2a241b8d68e0b32e9475233d6f830940e43a8980975edd097e')
  })
})

describe.each([
  ['hashLink', hashLink],
  ['hashLinkSync', hashLinkSync]
])('%s', (_, fn) => {
  const { join } = makeTestTree({
    foo: treeLink('testing'),
    bar: treeLink('testing'),
    boo: treeLink('testingtesting')
  })
  it('hashes symlinks', async () => {
    const foo = await fn(join('foo'))
    const bar = await fn(join('bar'))
    const boo = await fn(join('boo'))
    expect(foo).toMatch(/^[a-f0-9]{40}$/)
    expect(bar).toMatch(/^[a-f0-9]{40}$/)
    expect(boo).toMatch(/^[a-f0-9]{40}$/)
    expect(foo).toEqual(bar)
    expect(foo).not.toEqual(boo)
  })
  it('accepts custom algorithm', async () => {
    const foo = await fn(join('foo'), 'sha256')
    const bar = await fn(join('bar'), 'sha256')
    const boo = await fn(join('boo'), 'sha256')
    expect(foo).toMatch(/^[a-f0-9]{64}$/)
    expect(bar).toMatch(/^[a-f0-9]{64}$/)
    expect(boo).toMatch(/^[a-f0-9]{64}$/)
    expect(foo).toEqual(bar)
    expect(foo).not.toEqual(boo)
  })
  it('rejects unknown algorithm', async () => {
    await expect(async () => {
      await fn(join('foo'), 'whatever')
    }).rejects.toThrow('not supported')
  })
  it('rejects missing link', async () => {
    await expect(async () => {
      await fn(join('baz'))
    }).rejects.toThrow('ENOENT')
  })
  it('returns expected values', async () => {
    expect(await fn(join('foo'))).toEqual('dc724af18fbdd4e59189f5fe768a5f8311527050')
    expect(await fn(join('bar'))).toEqual('dc724af18fbdd4e59189f5fe768a5f8311527050')
    expect(await fn(join('boo'))).toEqual('b0212be2cc6081fba3e0b6f3dc6e0109d6f7b4cb')
    expect(await fn(join('foo'), 'sha256')).toEqual('cf80cd8aed482d5d1527d7dc72fceff84e6326592848447d2dc0b0e87dfc9a90')
    expect(await fn(join('bar'), 'sha256')).toEqual('cf80cd8aed482d5d1527d7dc72fceff84e6326592848447d2dc0b0e87dfc9a90')
    expect(await fn(join('boo'), 'sha256')).toEqual('ae80976b013f1a2a241b8d68e0b32e9475233d6f830940e43a8980975edd097e')
  })
})

describe.each([
  ['hashDir', hashDir],
  ['hashDirSync', hashDirSync]
])('%s', (_, fn) => {
  const { join } = makeTestTree({
    foo: treeDir({
      baz: treeFile('testing'),
      qux: treeLink('testing'),
      lol: treeDir()
    }),
    bar: treeDir({
      baz: treeFile('testing'),
      qux: treeLink('testing'),
      lol: treeDir()
    }),
    boo: treeDir({
      baz: treeFile('testing'),
      qux: treeLink('testing')
    }),
    baz: treeDir({
      baz: treeLink('testing'),
      qux: treeFile('testing')
    })
  })
  it('hashes directories', async () => {
    const foo = await fn(join('foo'))
    const bar = await fn(join('bar'))
    const boo = await fn(join('boo'))
    const baz = await fn(join('baz'))
    expect(foo).toMatch(/^[a-f0-9]{40}$/)
    expect(bar).toMatch(/^[a-f0-9]{40}$/)
    expect(boo).toMatch(/^[a-f0-9]{40}$/)
    expect(baz).toMatch(/^[a-f0-9]{40}$/)
    expect(foo).toEqual(bar)
    expect(foo).not.toEqual(boo)
    expect(boo).not.toEqual(baz)
  })
  it('accepts custom algorithm', async () => {
    const foo = await fn(join('foo'), 'sha256')
    const bar = await fn(join('bar'), 'sha256')
    const boo = await fn(join('boo'), 'sha256')
    expect(foo).toMatch(/^[a-f0-9]{64}$/)
    expect(bar).toMatch(/^[a-f0-9]{64}$/)
    expect(boo).toMatch(/^[a-f0-9]{64}$/)
    expect(foo).toEqual(bar)
    expect(foo).not.toEqual(boo)
  })
  it('rejects unknown algorithm', async () => {
    await expect(async () => {
      await fn(join('foo'), 'whatever')
    }).rejects.toThrow('not supported')
  })
  it('rejects missing directory', async () => {
    await expect(async () => {
      await fn(join('qux'))
    }).rejects.toThrow('ENOENT')
  })
  it('returns expected values', async () => {
    expect(await fn(join('foo'))).toEqual('b14f4d42d552d3790bf25b4e727b222983c17ee0')
    expect(await fn(join('bar'))).toEqual('b14f4d42d552d3790bf25b4e727b222983c17ee0')
    expect(await fn(join('boo'))).toEqual('3794b67fe5917590ce0016ee34066e18f4f3e60b')
    expect(await fn(join('baz'))).toEqual('1859248597419c01d242eb1014e53bfda0cebe0b')
    expect(await fn(join('foo'), 'sha256')).toEqual('d350335d735dc41e8aa5a6ea1a283b846141445f0f76c3bbb8276e90f55c2d27')
    expect(await fn(join('bar'), 'sha256')).toEqual('d350335d735dc41e8aa5a6ea1a283b846141445f0f76c3bbb8276e90f55c2d27')
    expect(await fn(join('boo'), 'sha256')).toEqual('d72eeb489631410a3cd4c80412ac99d019757fb5925f9e97fc6f11caf5d07acc')
    expect(await fn(join('baz'), 'sha256')).toEqual('b7edae72d42ecfc9ae38b3548ccbe487c10c52513b47c617cc5ad281a09ff5f5')
  })
})

describe.each([
  ['walkDir', walkDir],
  ['walkDirSync', walkDirSync]
])('%s', (_, fn) => {
  const { path } = makeTestTree({
    foo: treeFile('testing'),
    bar: treeLink('testing'),
    boo: treeDir({
      baz: treeDir({
        qux: treeFile('testing')
      }),
      lol: treeFile('testing')
    }),
    nah: treeDir()
  })
  it('walks directories', async () => {
    const { path } = makeTestTree({
      foo: treeFile('testing'),
      bar: treeLink('testing'),
      boo: treeDir({})
    })
    const walked: Walked[] = []
    for await (const w of fn(path)) {
      walked.push(w)
    }
    expect(sorted(walked)).toEqual(sorted([
      walkedFile('foo', '.'),
      walkedLink('bar', '.'),
      walkedDir('boo', '.')
    ]))
  })
  it('walks sub-directories', async () => {
    const walked: Walked[] = []
    for await (const w of fn(path)) {
      walked.push(w)
    }
    expect(sorted(walked)).toEqual(sorted([
      walkedFile('foo', '.'),
      walkedLink('bar', '.'),
      walkedDir('boo', '.'),
      walkedDir('baz', 'boo'),
      walkedFile('qux', 'boo/baz'),
      walkedFile('lol', 'boo'),
      walkedDir('nah', '.')
    ]))
  })
  it('walks sub-directories separarely if asked', async () => {
    const outer: Walked[] = []
    const inner: Walked[] = []
    const innerSync: Walked[] = []
    for await (const o of fn(path)) {
      outer.push(o)
      if (o.isDir && o.name === 'boo') {
        for await (const i of o.walk()) {
          inner.push(i)
        }
        for await (const i of o.walkSync()) {
          innerSync.push(i)
        }
      }
    }
    expect(sorted(outer)).toEqual(sorted([
      walkedFile('foo', '.'),
      walkedLink('bar', '.'),
      walkedDir('boo', '.'),
      walkedDir('nah', '.')
    ]))
    expect(sorted(inner)).toEqual(sorted([
      walkedDir('baz', 'boo'),
      walkedFile('qux', 'boo/baz'),
      walkedFile('lol', 'boo')
    ]))
    expect(sorted(innerSync)).toEqual(sorted([
      walkedDir('baz', 'boo'),
      walkedFile('qux', 'boo/baz'),
      walkedFile('lol', 'boo')
    ]))
  })
  it('skips sub-directories if asked', async () => {
    const walked: Walked[] = []
    for await (const w of fn(path)) {
      walked.push(w)
      if (w.isDir && w.name === 'baz') {
        w.skip()
      }
    }
    expect(sorted(walked)).toEqual(sorted([
      walkedFile('foo', '.'),
      walkedLink('bar', '.'),
      walkedDir('boo', '.'),
      walkedDir('baz', 'boo'),
      walkedFile('lol', 'boo'),
      walkedDir('nah', '.')
    ]))
  })
  it('rejects missing directory', async () => {
    const { join } = makeTestDir()
    await expect(async () => {
      /* eslint-disable-next-line @typescript-eslint/no-unused-vars, no-empty */
      for await (const _ of fn(join('nope'))) {}
    }).rejects.toThrow('ENOENT')
  })
})

describe.each([
  ['matchDir', matchDir],
  ['matchDirSync', matchDirSync]
])('%s', (_, fn) => {
  const { path } = makeTestTree({
    foo: treeFile('testing'),
    bar: treeLink('testing'),
    boo: treeDir({
      baz: treeDir({
        qux: treeFile('testing')
      }),
      lol: treeFile('testing')
    }),
    nah: treeDir()
  })
  const pats = Pattern.from([
    'foo',
    'boo/*',
    'b*',
    { src: '**/q*', not: '**/qux' }
  ])
  it('walks matches in directories', async () => {
    const matched: Matched[] = []
    for await (const m of fn(pats, path)) {
      matched.push(m)
    }
    expect(sorted(matched)).toEqual(sorted([
      matchedFile('foo', '.', pats[0]),
      matchedLink('bar', '.', pats[2]),
      matchedDir('boo', '.', pats[2]),
      matchedDir('baz', 'boo', pats[1]),
      matchedFile('lol', 'boo', pats[1])
    ]))
  })
  it('walks matched sub-directories if asked', async () => {
    const walked: Walked[] = []
    const walkedSync: Walked[] = []
    for await (const m of fn(pats, path)) {
      if (m.isDir && m.name === 'boo') {
        for await (const i of m.walk()) {
          walked.push(i)
        }
        for await (const i of m.walkSync()) {
          walkedSync.push(i)
        }
      }
    }
    expect(sorted(walked)).toEqual(sorted([
      walkedDir('baz', '.'),
      walkedFile('qux', 'baz'),
      walkedFile('lol', '.')
    ]))
    expect(sorted(walked)).toEqual(sorted([
      walkedDir('baz', '.'),
      walkedFile('qux', 'baz'),
      walkedFile('lol', '.')
    ]))
  })
  it('rejects missing directory', async () => {
    const { join } = makeTestDir()
    await expect(async () => {
      /* eslint-disable-next-line @typescript-eslint/no-unused-vars, no-empty */
      for await (const _ of fn(pats, join('nope'))) {}
    }).rejects.toThrow('ENOENT')
  })
})

describe.each([
  ['copyMatches', copyMatches],
  ['copyMatchesSync', copyMatchesSync]
])('%s', (_, fn) => {
  it('copies matches from src to dst', async () => {
    const pats = Pattern.from([
      { dst: 'pat1', src: 'qux*' },
      { dst: 'pat2', src: '**/qux*' },
      { dst: 'pat3', src: 'boo' },
      { dst: 'pat4', src: 'boo/*' },
      { dst: 'pat5', src: 'boo/**' },
      { dst: 'pat6', src: '**/q*', not: ['**/qux1', 'qux4'] }
    ])
    const { path, join, tree } = makeTestTree({
      src: treeDir({
        foo: treeFile('testing'),
        bar: treeLink('testing'),
        boo: treeDir({
          baz: treeDir({
            qux1: treeFile('testing1'),
            qux2: treeFile('testing2')
          }),
          lol: treeFile('testing'),
          qux3: treeFile('testing3')
        }),
        nah: treeDir(),
        qux4: treeFile('testing4')
      })
    })
    await fn(pats, join('src'), join('dst'))
    expect(readTree(path)).toStrictEqual({
      src: tree.src,
      dst: treeDir({
        pat1: treeDir({
          qux4: treeFile('testing4')
        }),
        pat2: treeDir({
          qux1: treeFile('testing1'),
          qux2: treeFile('testing2'),
          qux3: treeFile('testing3'),
          qux4: treeFile('testing4')
        }),
        pat3: treeDir({
          boo: treeDir({
            baz: treeDir({
              qux1: treeFile('testing1'),
              qux2: treeFile('testing2')
            }),
            lol: treeFile('testing'),
            qux3: treeFile('testing3')
          })
        }),
        pat4: treeDir({
          baz: treeDir({
            qux1: treeFile('testing1'),
            qux2: treeFile('testing2')
          }),
          lol: treeFile('testing'),
          qux3: treeFile('testing3')
        }),
        pat5: treeDir({
          baz: treeDir({
            qux1: treeFile('testing1'),
            qux2: treeFile('testing2')
          }),
          lol: treeFile('testing'),
          qux3: treeFile('testing3')
        }),
        pat6: treeDir({
          qux2: treeFile('testing2'),
          qux3: treeFile('testing3')
        })
      })
    })
  })
  it('rejects missing src', async () => {
    const { join } = makeTestDir()
    await expect(async () => {
      await fn([], join('src'), join('dst'))
    }).rejects.toThrow('ENOENT')
  })
  it('accepts existing dst', async () => {
    const { join } = makeTestTree({
      src: treeDir(),
      dst: treeDir()
    })
    await fn([], join('src'), join('dst'))
  })
  it('accepts existing files in dst', async () => {
    const pats = Pattern.from(['*'])
    const { join } = makeTestTree({
      src: treeDir({ foo: treeFile('testing') }),
      dst: treeDir({ bar: treeFile('testing') })
    })
    await fn(pats, join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeFile('testing'),
      bar: treeFile('testing')
    })
  })
  it('accepts existing links in dst', async () => {
    const pats = Pattern.from(['*'])
    const { join } = makeTestTree({
      src: treeDir({ foo: treeLink('testing') }),
      dst: treeDir({ bar: treeLink('testing') })
    })
    await fn(pats, join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeLink('testing'),
      bar: treeLink('testing')
    })
  })
  it('accepts existing dirs in dst', async () => {
    const pats = Pattern.from(['*'])
    const { join } = makeTestTree({
      src: treeDir({ foo: treeDir() }),
      dst: treeDir({ bar: treeDir() })
    })
    await fn(pats, join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeDir(),
      bar: treeDir()
    })
  })
  it('rejects overlapping files in dst', async () => {
    const pats = Pattern.from(['*'])
    const { join } = makeTestTree({
      src: treeDir({ foo: treeFile('testing') }),
      dst: treeDir({ foo: treeFile('testing') })
    })
    await expect(async () => {
      await fn(pats, join('src'), join('dst'))
    }).rejects.toThrow('EEXIST')
  })
  it('rejects overlapping links in dst', async () => {
    const pats = Pattern.from(['*'])
    const { join } = makeTestTree({
      src: treeDir({ foo: treeLink('testing') }),
      dst: treeDir({ foo: treeLink('testing') })
    })
    await expect(async () => {
      await fn(pats, join('src'), join('dst'))
    }).rejects.toThrow('EEXIST')
  })
  it('accepts overlapping dirs in dst', async () => {
    const pats = Pattern.from(['*'])
    const { join } = makeTestTree({
      src: treeDir({
        foo: treeDir({ bar: treeFile('testing') })
      }),
      dst: treeDir({
        foo: treeDir({ boo: treeFile('testing') })
      })
    })
    await fn(pats, join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeDir({
        bar: treeFile('testing'),
        boo: treeFile('testing')
      })
    })
  })
})

describe.each([
  ['linkMatches', linkMatches],
  ['linkMatchesSync', linkMatchesSync]
])('%s', (_, fn) => {
  it('links matches from src to dst', async () => {
    const pats = Pattern.from([
      { dst: 'pat1', src: 'qux*' },
      { dst: 'pat2', src: '**/qux*' },
      { dst: 'pat3', src: 'boo' },
      { dst: 'pat4', src: 'boo/*' },
      { dst: 'pat5', src: 'boo/**' },
      { dst: 'pat6', src: '**/q*', not: ['**/qux1', 'qux4'] }
    ])
    const { join } = makeTestTree({
      src: treeDir({
        foo: treeFile('testing'),
        bar: treeLink('testing'),
        boo: treeDir({
          baz: treeDir({
            qux1: treeFile('testing1'),
            qux2: treeFile('testing2')
          }),
          lol: treeFile('testing'),
          qux3: treeFile('testing3')
        }),
        nah: treeDir(),
        qux4: treeFile('testing4')
      })
    })
    await fn(pats, join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      pat1: treeDir({
        qux4: treeFile('testing4')
      }),
      pat2: treeDir({
        qux1: treeFile('testing1'),
        qux2: treeFile('testing2'),
        qux3: treeFile('testing3'),
        qux4: treeFile('testing4')
      }),
      pat3: treeDir({
        boo: treeDir({
          baz: treeDir({
            qux1: treeFile('testing1'),
            qux2: treeFile('testing2')
          }),
          lol: treeFile('testing'),
          qux3: treeFile('testing3')
        })
      }),
      pat4: treeDir({
        baz: treeDir({
          qux1: treeFile('testing1'),
          qux2: treeFile('testing2')
        }),
        lol: treeFile('testing'),
        qux3: treeFile('testing3')
      }),
      pat5: treeDir({
        baz: treeDir({
          qux1: treeFile('testing1'),
          qux2: treeFile('testing2')
        }),
        lol: treeFile('testing'),
        qux3: treeFile('testing3')
      }),
      pat6: treeDir({
        qux2: treeFile('testing2'),
        qux3: treeFile('testing3')
      })
    })
    appendFileSync(join('src', 'boo', 'baz', 'qux1'), 'testing')
    appendFileSync(join('src', 'boo', 'baz', 'qux2'), 'testing')
    expect(readTree(join('dst'))).toStrictEqual({
      pat1: treeDir({
        qux4: treeFile('testing4')
      }),
      pat2: treeDir({
        qux1: treeFile('testing1testing'),
        qux2: treeFile('testing2testing'),
        qux3: treeFile('testing3'),
        qux4: treeFile('testing4')
      }),
      pat3: treeDir({
        boo: treeDir({
          baz: treeDir({
            qux1: treeFile('testing1testing'),
            qux2: treeFile('testing2testing')
          }),
          lol: treeFile('testing'),
          qux3: treeFile('testing3')
        })
      }),
      pat4: treeDir({
        baz: treeDir({
          qux1: treeFile('testing1testing'),
          qux2: treeFile('testing2testing')
        }),
        lol: treeFile('testing'),
        qux3: treeFile('testing3')
      }),
      pat5: treeDir({
        baz: treeDir({
          qux1: treeFile('testing1testing'),
          qux2: treeFile('testing2testing')
        }),
        lol: treeFile('testing'),
        qux3: treeFile('testing3')
      }),
      pat6: treeDir({
        qux2: treeFile('testing2testing'),
        qux3: treeFile('testing3')
      })
    })
  })
  it('rejects missing src', async () => {
    const { join } = makeTestDir()
    await expect(async () => {
      await fn([], join('src'), join('dst'))
    }).rejects.toThrow('ENOENT')
  })
  it('accepts existing dst', async () => {
    const { join } = makeTestTree({
      src: treeDir(),
      dst: treeDir()
    })
    await fn([], join('src'), join('dst'))
  })
  it('accepts existing files in dst', async () => {
    const pats = Pattern.from(['*'])
    const { join } = makeTestTree({
      src: treeDir({ foo: treeFile('testing') }),
      dst: treeDir({ bar: treeFile('testing') })
    })
    await fn(pats, join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeFile('testing'),
      bar: treeFile('testing')
    })
  })
  it('accepts existing links in dst', async () => {
    const pats = Pattern.from(['*'])
    const { join } = makeTestTree({
      src: treeDir({ foo: treeLink('testing') }),
      dst: treeDir({ bar: treeLink('testing') })
    })
    await fn(pats, join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeLink('testing'),
      bar: treeLink('testing')
    })
  })
  it('accepts existing dirs in dst', async () => {
    const pats = Pattern.from(['*'])
    const { join } = makeTestTree({
      src: treeDir({ foo: treeDir() }),
      dst: treeDir({ bar: treeDir() })
    })
    await fn(pats, join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeDir(),
      bar: treeDir()
    })
  })
  it('rejects overlapping files in dst', async () => {
    const pats = Pattern.from(['*'])
    const { join } = makeTestTree({
      src: treeDir({ foo: treeFile('testing') }),
      dst: treeDir({ foo: treeFile('testing') })
    })
    await expect(async () => {
      await fn(pats, join('src'), join('dst'))
    }).rejects.toThrow('EEXIST')
  })
  it('rejects overlapping links in dst', async () => {
    const pats = Pattern.from(['*'])
    const { join } = makeTestTree({
      src: treeDir({ foo: treeLink('testing') }),
      dst: treeDir({ foo: treeLink('testing') })
    })
    await expect(async () => {
      await fn(pats, join('src'), join('dst'))
    }).rejects.toThrow('EEXIST')
  })
  it('accepts overlapping dirs in dst', async () => {
    const pats = Pattern.from(['*'])
    const { join } = makeTestTree({
      src: treeDir({
        foo: treeDir({ bar: treeFile('testing') })
      }),
      dst: treeDir({
        foo: treeDir({ boo: treeFile('testing') })
      })
    })
    await fn(pats, join('src'), join('dst'))
    expect(readTree(join('dst'))).toStrictEqual({
      foo: treeDir({
        bar: treeFile('testing'),
        boo: treeFile('testing')
      })
    })
  })
})

describe('Pattern', () => {
  it('supports hashValue', () => {
    expectUniqueHashes(6, [
      hashValue(new Pattern('foo')),
      hashValue(new Pattern('foo', 'bar')),
      hashValue(new Pattern('foo', 'bar', 'boo')),
      hashValue(new Pattern('foo', 'bar', ['boo'])),
      hashValue(new Pattern('foo', 'bar', ['boo', 'baz'])),
      hashValue(Pattern.from(['foo', 'bar'])),
      hashValue(Pattern.from([{ src: 'foo' }, { src: 'bar' }])),
      hashValue(Pattern.from([{ src: 'foo' }, { src: 'bar', not: 'boo' }])),
      hashValue(Pattern.from([{ src: 'foo' }, { src: 'bar', not: ['boo'] }]))
    ])
  })
  it('uses custom rendering with inspect', () => {
    expect(inspect(new Pattern('foo')))
      .toMatch(/Pattern\(\)\s*\{\s*src:\s*'foo'\s*\}/)
    expect(inspect(new Pattern('foo', 'bar')))
      .toMatch(/Pattern\(\)\s*\{\s*src:\s*'foo',\s*dst:\s*'bar'\s*\}/)
    expect(inspect(new Pattern('foo', 'bar', 'boo')))
      .toMatch(/Pattern\(\)\s*\{\s*src:\s*'foo',\s*dst:\s*'bar',\s*not:\s*\[\s*'boo'\s*\]\s*\}/)
  })
})

function sorted<T extends {
  name: string
  path: string
}> (named: T[]): T[] {
  return named.sort((a, b) => {
    const cmp = a.path.localeCompare(b.path)
    return cmp === 0
      ? a.name.localeCompare(b.name)
      : cmp
  })
}

function walkedFile (name: string, path: string): Walked {
  return {
    name,
    path,
    isDir: false,
    isLink: false
  }
}

function walkedLink (name: string, path: string): Walked {
  return {
    name,
    path,
    isDir: false,
    isLink: true
  }
}

function walkedDir (name: string, path: string): Walked {
  return {
    name,
    path,
    isDir: true,
    isLink: false,
    skip: expect.any(Function),
    walk: expect.any(Function),
    walkSync: expect.any(Function)
  }
}

function matchedFile (name: string, path: string, ...pats: Pattern[]): Matched {
  return { ...walkedFile(name, path), pats }
}

function matchedLink (name: string, path: string, ...pats: Pattern[]): Matched {
  return { ...walkedLink(name, path), pats }
}

function matchedDir (name: string, path: string, ...pats: Pattern[]): Matched {
  return {
    name,
    path,
    pats,
    isDir: true,
    isLink: false,
    walk: expect.any(Function),
    walkSync: expect.any(Function)
  }
}

function expectUniqueHashes (count: number, hashes: string[]): void {
  const set = new Set(hashes)
  expect(set.size).toEqual(count)
  for (const hash of set) {
    expect(hash).toMatch(/^[a-f0-9]{40}$/)
  }
}
