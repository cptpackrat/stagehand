import { matcher } from 'micromatch'
import { createHash, Hash } from 'crypto'
import { basename, dirname, join } from 'path'
import { inspect, InspectOptions } from 'util'
import {
  constants,
  copyFileSync as _copyFileSync,
  createReadStream,
  Dirent,
  linkSync,
  mkdirSync,
  readdirSync,
  readFileSync,
  readlinkSync,
  rmSync,
  symlinkSync
} from 'fs'
import {
  copyFile as _copyFile,
  link,
  mkdir,
  readdir,
  readlink,
  rm,
  symlink
} from 'fs/promises'

/** Copy a file from one location to another; wrapped version of `copyFile`
  * from `fs/promises` that will always fail if the destination path exists.
  * @param src Source path.
  * @param dst Destination path. */
export async function copyFile (src: string, dst: string): Promise<void> {
  return await _copyFile(src, dst, constants.COPYFILE_EXCL)
}

/** Copy a file synchronously from one location to another; wrapped version
  * of `copyFileSync` from `fs` that will always fail if the destination
  * path exists.
  * @param src Source path.
  * @param dst Destination path. */
export function copyFileSync (src: string, dst: string): void {
  return _copyFileSync(src, dst, constants.COPYFILE_EXCL)
}

/** Copy a symlink from one location to another, rather than copying
  * the file or directory the symlink points to. Will fail if the
  * destination path already exists.
  * @param src Source path.
  * @param dst Destination path. */
export async function copyLink (src: string, dst: string): Promise<void> {
  return await symlink(await readlink(src), dst)
}

/** Copy a symlink synchronously from one location to another, rather
  * than copying the file or directory the symlink points to. Will
  * fail if the destination path already exists.
  * @param src Source path.
  * @param dst Destination path. */
export function copyLinkSync (src: string, dst: string): void {
  return symlinkSync(readlinkSync(src), dst)
}

/** Copy the contents of a directory from one location to another;
  * any symlinks encountered will be copied via `copyLink`, files
  * via `copyFile`, and sub-directories by recursing into itself.
  * @details In the interest of avoiding side-effects when overlaying files
  *          from multiple sources, this function adheres to specific rules
  *          when copying to a destination directory that already exists.
  *          Attempting to copy any file or symlink that would overwrite an
  *          existing path in the destination directory will fail. Copying
  *          a sub-directory over an existing path will succeed, provided
  *          that the destination path is also a directory, in which case
  *          the contents of the two will be merged following the same rules.
  * @param src Source path.
  * @param dst Destination path. */
export async function copyDir (src: string, dst: string): Promise<void> {
  await mkdir(dst, { recursive: true })
  for (const entry of await readdir(src, { withFileTypes: true })) {
    if (entry.isDirectory()) {
      await copyDir(join(src, entry.name), join(dst, entry.name))
    } else if (entry.isSymbolicLink()) {
      await copyLink(join(src, entry.name), join(dst, entry.name))
    } else {
      await copyFile(join(src, entry.name), join(dst, entry.name))
    }
  }
}

/** Copy the contents of a directory synchronously from one location to
  * another; any symlinks encountered will be copied via `copyLinkSync`,
  * files via `copyFileSync`, and sub-directories by recursing into itself.
  * @details In the interest of avoiding side-effects when overlaying files
  *          from multiple sources, this function adheres to specific rules
  *          when copying to a destination directory that already exists.
  *          Attempting to copy any file or symlink that would overwrite an
  *          existing path in the destination directory will fail. Copying
  *          a sub-directory over an existing path will succeed, provided
  *          that the destination path is also a directory, in which case
  *          the contents of the two will be merged following the same rules.
  * @param src Source path.
  * @param dst Destination path. */
export function copyDirSync (src: string, dst: string): void {
  mkdirSync(dst, { recursive: true })
  for (const entry of readdirSync(src, { withFileTypes: true })) {
    if (entry.isDirectory()) {
      copyDirSync(join(src, entry.name), join(dst, entry.name))
    } else if (entry.isSymbolicLink()) {
      copyLinkSync(join(src, entry.name), join(dst, entry.name))
    } else {
      copyFileSync(join(src, entry.name), join(dst, entry.name))
    }
  }
}

/** Companion to `copyFile`; hardlink a file from one location to another rather
  * than copying it. Will fail if the destination path exists.
  * @note This function is a re-export of `link` from `fs/promises`; it exists
  *       purely for the sake of providing a consistent API from this module.
  * @param src Source path.
  * @param dst Destination path. */
export const linkFile = link as (src: string, dst: string) => Promise<void>

/** Companion to `copyFileSync`; synchronously hardlink a file from one location
  * to another rather than copying it. Will fail if the destination path exists.
  * @note This function is a re-export `linkSync` from `fs`; it exists purely
  *       for the sake of providing a consistent API from this module.
  * @param src Source path.
  * @param dst Destination path. */
export const linkFileSync = linkSync as (src: string, dst: string) => void

/** Companion to `copyDir`; copy the contents of a directory from one location
  * to another, creating hardlinks rather than actual copies for any files or
  * symlinks encountered within.
  * @details In the interest of avoiding side-effects when overlaying files
  *          from multiple sources, this function adheres to specific rules
  *          when copying to a destination directory that already exists.
  *          Attempting to copy any file or symlink that would overwrite an
  *          existing path in the destination directory will fail. Copying
  *          a sub-directory over an existing path will succeed, provided
  *          that the destination path is also a directory, in which case
  *          the contents of the two will be merged following the same rules.
  * @param src Source path.
  * @param dst Destination path. */
export async function linkDir (src: string, dst: string): Promise<void> {
  await mkdir(dst, { recursive: true })
  for (const entry of await readdir(src, { withFileTypes: true })) {
    if (entry.isDirectory()) {
      await linkDir(join(src, entry.name), join(dst, entry.name))
    } else {
      await link(join(src, entry.name), join(dst, entry.name))
    }
  }
}

/** Companion to `copyDirSync`; synchronously copy the contents of a directory
  * from one location to another, creating hardlinks rather than actual copies
  * for any files or symlinks encountered within.
  * @details In the interest of avoiding side-effects when overlaying files
  *          from multiple sources, this function adheres to specific rules
  *          when copying to a destination directory that already exists.
  *          Attempting to copy any file or symlink that would overwrite an
  *          existing path in the destination directory will fail. Copying
  *          a sub-directory over an existing path will succeed, provided
  *          that the destination path is also a directory, in which case
  *          the contents of the two will be merged following the same rules.
  * @param src Source path.
  * @param dst Destination path. */
export function linkDirSync (src: string, dst: string): void {
  mkdirSync(dst, { recursive: true })
  for (const entry of readdirSync(src, { withFileTypes: true })) {
    if (entry.isDirectory()) {
      linkDirSync(join(src, entry.name), join(dst, entry.name))
    } else {
      linkSync(join(src, entry.name), join(dst, entry.name))
    }
  }
}

/** Ensure the specified directory exists, and is empty.
  * @param path Directory to empty. */
export async function emptyDir (path: string): Promise<void> {
  await mkdir(path, { recursive: true })
  for (const name of await readdir(path)) {
    await rm(join(path, name), { recursive: true, force: true })
  }
}

/** Synchronously ensure the specified directory exists, and is empty.
  * @param path Directory to empty. */
export function emptyDirSync (path: string): void {
  mkdirSync(path, { recursive: true })
  for (const name of readdirSync(path)) {
    rmSync(join(path, name), { recursive: true, force: true })
  }
}

/** Hash a simple javascript object, array, or primitive.
  * @param value Value to hash.
  * @param algo Hash algorithm; SHA1 by default.
  * @details
  * This function is intended for change detection in data structures, and in the interest
  * of minimising false positives/negatives arising from undefined or inconsistent behaviour
  * it is relatively strict about the range of acceptable inputs. Specifically, it will accept:
  * - primitive-like values including `bigint`, `boolean`, `null`, `number`, `string`, and `undefined`,
  * - plain objects; i.e. where `Object.getPrototype(value) === Object.prototype`,
  * - plain arrays; i.e. where `Object.getPrototype(value) === Array.prototype`,
  * - the built-ins `Buffer`, `Date`, `Map`, `RegExp` and `Set` where, as above,
  *   the object is an instance of the base class for these objects, and
  * - any object or class that implements `hashValue.Custom` (including instances of `Pattern`).
  *
  * It will explicitly reject `symbol`, `function`, and any object that:
  * - does not implement `hashValue.Custom`, and
  * - does not use one of the prototypes listed above.
  *
  * The hashes produced by this function are intended to be consistent regardless of
  * how the input data was constructed. To this end it will:
  * - sort the contents of objects, arrays, maps, and sets before hashing them to
  *   minimise false positives from variations in insertion/iteration order, and
  * - treat object properties and array elements containing `undefined` as though
  *   they were non-existent to minimise false positives due to variations in
  *   deletion strategy.
  *
  * The hashes produced by this function are also intended to be type-aware, and
  * it will, for example, produce different results when hashing:
  * - a `string` vs a `Buffer` containing the same value,
  * - a `number` vs a `bigint` containing the same value,
  * - any other primitive-like vs a `string` encoding of the same value, or
  * - any `object`, `array`, or `Map` vs the same set of elements/properties in a
  *   collection of a different type. */
/* eslint-disable-next-line import/export */
export function hashValue (value: any, algo: string = 'sha1'): string {
  return _hashValue(value, createHash(algo), [])
    .digest()
    .toString('hex')
}

/* eslint-disable-next-line import/export,@typescript-eslint/no-namespace */
export namespace hashValue {
  /** Allows objects to provide custom behaviour when passed to `hashValue`. */
  export const custom = Symbol('hashValue.custom')
  /** Interface for an object that uses custom behaviour when passed to `hashValue`. */
  export interface Custom {
    /** Allows objects to provide custom behaviour when passed to `hashValue`.
      * @param hash A function to receive any data that contributes to the
      *             hash of the custom object; may be called as many times
      *             as necessary. */
    [hashValue.custom]: (hash: (value: any) => void) => void
  }
}

/** Process a value into an existing hash. */
function _hashValue (value: any, hash: Hash, stack: object[]): Hash {
  switch (typeof value) {
    case 'undefined':
      return hash.update('undefined')
    case 'bigint':
    case 'boolean':
    case 'number':
    case 'string':
      return hash
        .update(typeof value)
        .update(value.toString())
    case 'object':
      if (value === null) {
        return hash.update('null')
      }
      if (stack.includes(value)) {
        throw new Error('cannot hash cyclic object')
      }
      if (value[hashValue.custom] !== undefined) {
        return _hashCustom(value, hash, [...stack, value])
      }
      switch (Object.getPrototypeOf(value)) {
        case Array.prototype:
          return _hashPlain(value, 'array', hash, [...stack, value])
        case Buffer.prototype:
          return _hashBuffer(value, hash)
        case Date.prototype:
          return _hashDate(value, hash)
        case Map.prototype:
          return _hashMap(value, hash, [...stack, value])
        case Object.prototype:
          return _hashPlain(value, 'object', hash, [...stack, value])
        case RegExp.prototype:
          return _hashRegExp(value, hash)
        case Set.prototype:
          return _hashSet(value, hash, [...stack, value])
        default: {
          const name: string = Object.getPrototypeOf(value).constructor?.name ?? 'object'
          throw new Error('cannot hash ' + name)
        }
      }
    default:
      throw new Error('cannot hash ' + typeof value)
  }
}

/** Process a plain object or array into an existing hash. */
function _hashPlain (obj: any, name: string, hash: Hash, stack: object[]): Hash {
  hash.update(name)
  for (const key of Object.keys(obj).sort()) {
    const val = obj[key]
    if (val !== undefined) {
      _hashValue(key, hash, stack)
      _hashValue(val, hash, stack)
    }
  }
  return hash.update('end')
}

/** Process a customised object into an existing hash. */
function _hashCustom (custom: hashValue.Custom, hash: Hash, stack: object[]): Hash {
  hash.update('custom')
  custom[hashValue.custom]((val: any) => _hashValue(val, hash, stack))
  return hash.update('end')
}

/** Process a buffer into an existing hash. */
function _hashBuffer (buffer: Buffer, hash: Hash): Hash {
  return hash
    .update('buffer')
    .update(buffer)
}

/** Process a date into an existing hash. */
function _hashDate (date: Date, hash: Hash): Hash {
  return hash
    .update('date')
    .update(date.getTime().toString())
}

/** Process a map into an existing hash. */
function _hashMap (map: Map<any, any>, hash: Hash, stack: object[]): Hash {
  hash.update('map')
  /* eslint-disable-next-line @typescript-eslint/require-array-sort-compare */
  for (const key of [...map.keys()].sort()) {
    _hashValue(key, hash, stack)
    _hashValue(map.get(key), hash, stack)
  }
  return hash.update('end')
}

/** Process a regex into an existing hash. */
function _hashRegExp (regexp: RegExp, hash: Hash): Hash {
  return hash
    .update('regexp')
    .update(regexp.source)
}

/** Process a set into an existing hash. */
function _hashSet (set: Set<any>, hash: Hash, stack: object[]): Hash {
  hash.update('set')
  /* eslint-disable-next-line @typescript-eslint/require-array-sort-compare */
  for (const key of [...set.keys()].sort()) {
    _hashValue(key, hash, stack)
  }
  return hash.update('end')
}

/** Hash the contents of a file.
  * @param path Path to hash.
  * @param algo Hash algorithm; SHA1 by default. */
export async function hashFile (path: string, algo: string = 'sha1'): Promise<string> {
  const hash = createHash(algo)
  for await (const data of createReadStream(path)) {
    hash.update(data)
  }
  return hash.digest('hex')
}

/** Synchronously hash the contents of a file.
  * @param path Path to hash.
  * @param algo Hash algorithm; SHA1 by default. */
export function hashFileSync (path: string, algo: string = 'sha1'): string {
  return createHash(algo)
    .update(readFileSync(path))
    .digest('hex')
}

/** Hash the contents of a symlink, rather than the file it points to.
  * @param path Path to hash.
  * @param algo Hash algorithm; SHA1 by default. */
export async function hashLink (path: string, algo: string = 'sha1'): Promise<string> {
  return createHash(algo)
    .update(await readlink(path))
    .digest('hex')
}

/** Synchronously hash the contents of a symlink, rather than the file it points to.
  * @param path Path to hash.
  * @param algo Hash algorithm; SHA1 by default. */
export function hashLinkSync (path: string, algo: string = 'sha1'): string {
  return createHash(algo)
    .update(readlinkSync(path))
    .digest('hex')
}

/** Hash the contents of a directory; symlinks encountered will be hashed
  * via `hashLink`, files via `hashFile`, and sub-directories by recursing
  * into itself. Directory entries are sorted to ensure consistent results.
  * @param path Path to hash.
  * @param algo Hash algorithm; SHA1 by default. */
export async function hashDir (path: string, algo: string = 'sha1'): Promise<string> {
  const hash = createHash(algo)
  for (const entry of await readdirSorted(path)) {
    const full = join(path, entry.name)
    hash
      .update(entry.name)
      .update(entry.isDirectory()
        ? 'dir' + await hashDir(full, algo)
        : entry.isSymbolicLink()
          ? 'link' + await hashLink(full, algo)
          : 'file' + await hashFile(full, algo))
  }
  return hash.digest('hex')
}

/** Synchronously hash the contents of a directory; symlinks encountered will
  * be hashed via `hashLinkSync`, files via `hashFileSync`, and sub-directories
  * by recursing into itself. Directory entries are sorted to ensure consistent
  * results.
  * @param path Path to hash.
  * @param algo Hash algorithm; SHA1 by default. */
export function hashDirSync (path: string, algo: string = 'sha1'): string {
  const hash = createHash(algo)
  for (const entry of readdirSortedSync(path)) {
    const full = join(path, entry.name)
    hash
      .update(entry.name)
      .update(entry.isDirectory()
        ? 'dir' + hashDirSync(full, algo)
        : entry.isSymbolicLink()
          ? 'link' + hashLinkSync(full, algo)
          : 'file' + hashFileSync(full, algo))
  }
  return hash.digest('hex')
}

/** Return a sorted directory listing; used by `hashDir` to ensure consistent
  * results regardless of the order returned by the underlying `readdir` call.
  * @param path Path to read. */
export async function readdirSorted (path: string): Promise<Dirent[]> {
  return (await readdir(path, { withFileTypes: true })).sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0)
}

/** Return a sorted directory listing; used by `hashDirSync` to ensure consistent
  * results regardless of the order returned by the underlying `readdirSync` call.
  * @param path Path to read. */
export function readdirSortedSync (path: string): Dirent[] {
  return readdirSync(path, { withFileTypes: true }).sort((a, b) => a.name < b.name ? -1 : a.name > b.name ? 1 : 0)
}

/** Represents a directory found during a walk. */
export interface WalkedDir {
  /** Name of entry. */
  name: string
  /** Enclosing directory. */
  path: string
  /** Is this a directory? */
  isDir: true
  /** Is this a symlink? */
  isLink: false
  /** Skip the contents of this directory when continuing the walk. */
  skip: () => void
  /** Walk the contents of this directory, then
    * skip it when continuing the original walk. */
  walk: () => AsyncGenerator<Walked>
  /** Walk the contents of this directory synchronously,
    * then skip it when continuing the original walk. */
  walkSync: () => Generator<Walked>
}

/** Represents a file or symlink found during a walk. */
export interface WalkedFile {
  /** Name of entry. */
  name: string
  /** Enclosing directory. */
  path: string
  /** Is this a directory? */
  isDir: false
  /** Is this a symlink? */
  isLink: boolean
}

/** Respresents a directory entry found during a walk. */
export type Walked = WalkedDir | WalkedFile

/** Walk through a directory tree, yielding details for each entry within.
  * @param path Path to walk. */
export function walkDir (path: string): AsyncGenerator<Walked> {
  return _walkDir(path, '.')
}

async function * _walkDir (root: string, path: string): AsyncGenerator<Walked> {
  for (const entry of await readdir(join(root, path), { withFileTypes: true })) {
    if (entry.isDirectory()) {
      let skipped = false
      yield _getWalkedDir(root, path, entry.name, () => { skipped = true })
      if (!skipped) {
        yield * _walkDir(root, join(path, entry.name))
      }
    } else {
      yield {
        name: entry.name,
        path,
        isDir: false,
        isLink: entry.isSymbolicLink()
      }
    }
  }
}

/** Synchronously walk through a directory tree, yielding details for each entry within.
  * @param path Path to walk. */
export function walkDirSync (path: string): Generator<Walked> {
  return _walkDirSync(path, '.')
}

function * _walkDirSync (root: string, path: string): Generator<Walked> {
  for (const entry of readdirSync(join(root, path), { withFileTypes: true })) {
    if (entry.isDirectory()) {
      let skipped = false
      yield _getWalkedDir(root, path, entry.name, () => { skipped = true })
      if (!skipped) {
        yield * _walkDirSync(root, join(path, entry.name))
      }
    } else {
      yield {
        name: entry.name,
        path,
        isDir: false,
        isLink: entry.isSymbolicLink()
      }
    }
  }
}

/** Return a `Walked` representing a given directory entry. */
function _getWalkedDir (root: string, path: string, name: string, skip: () => void): WalkedDir {
  return {
    name,
    path,
    isDir: true,
    isLink: false,
    skip,
    walk: () => {
      skip()
      return _walkDir(root, join(path, name))
    },
    walkSync: () => {
      skip()
      return _walkDirSync(root, join(path, name))
    }
  }
}

/** Pattern used to match paths in a source directory, and
  * optionally remap them to a new path in the destination
  * directory. */
export class Pattern implements hashValue.Custom {
  /** Source match pattern. */
  readonly src: string
  /** Source match exclusions. */
  readonly not?: string[]
  /** Destination directory. */
  readonly dst?: string

  constructor (src: string, dst?: string, not?: string | string[]) {
    this.src = src
    this.dst = dst
    this.not = typeof not === 'string' ? [not] : not
    this.matches = matcher(src, { strictSlashes: true, ignore: not })
    this.mapping = typeof dst === 'string'
      ? (path: string) => join(dst, basename(path))
      : (path: string) => path
  }

  /** Check if the given source path matches this pattern.
    * @param path Source path. */
  readonly matches: (path: string) => boolean

  /** Map a source path to its destination for this pattern.
    * @param path Source path. */
  readonly mapping: (path: string) => string

  /** Return compiled versions of an array of patterns,
    * specified either as a source match pattern or an
    * object containing a combination of source pattern
    * and destination path.
    * @param pats Patterns to compile. */
  static from (pats: Array<string | {
    src: string
    dst?: string
    not?: string | string[]
  }>): Pattern[] {
    return pats.map((pat) => typeof pat === 'string'
      ? new Pattern(pat)
      : new Pattern(pat.src, pat.dst, pat.not))
  }

  [hashValue.custom] (hash: (val: any) => void): void {
    hash(this.src)
    hash(this.dst)
    hash(this.not)
  }

  [inspect.custom] (_: number, opts: InspectOptions, insp: typeof inspect = inspect): string {
    return 'Pattern() ' + insp(this.not === undefined
      ? this.dst === undefined
        ? { src: this.src }
        : { src: this.src, dst: this.dst }
      : this.dst === undefined
        ? { src: this.src, not: this.not }
        : { src: this.src, dst: this.dst, not: this.not }, opts)
  }
}

/** Represents a directory matched during a walk. */
export interface MatchedDir {
  /** Patterns that matched this entry. */
  pats: Pattern[]
  /** Name of entry. */
  name: string
  /** Enclosing directory. */
  path: string
  /** Is this a directory? */
  isDir: true
  /** Is this a symlink? */
  isLink: false
  /** Walk the contents of this directory. */
  walk: () => AsyncGenerator<Walked>
  /** Walk the contents of this directory synchronously. */
  walkSync: () => Generator<Walked>
}

/** Represents a file or symlink matched during a walk. */
export interface MatchedFile {
  /** Patterns that matched this entry. */
  pats: Pattern[]
  /** Name of entry. */
  name: string
  /** Enclosing directory. */
  path: string
  /** Is this a directory? */
  isDir: false
  /** Is this a symlink? */
  isLink: boolean
}

/** Respresents a directory entry matched during a walk. */
export type Matched = MatchedDir | MatchedFile

/** Walk through a directory tree, yielding details for entries
  * that match one or more of a given set of patterns.
  * @param pats Patterns to match.
  * @param path Path to walk. */
export function matchDir (pats: Pattern[], path: string): AsyncGenerator<Matched> {
  return _matchDir(pats, path, '.')
}

async function * _matchDir (pats: Pattern[], root: string, path: string): AsyncGenerator<Matched> {
  for (const entry of await readdir(join(root, path), { withFileTypes: true })) {
    const { yay, nay } = _checkPatterns(pats, join(path, entry.name))
    if (yay.length > 0) {
      yield _getMatched(yay, root, path, entry)
    }
    if (nay.length > 0 && entry.isDirectory()) {
      yield * _matchDir(nay, root, join(path, entry.name))
    }
  }
}

/** Synchronously walk through a directory tree, yielding details
  * for entries that match one or more of a given set of patterns.
  * @param pats Patterns to match.
  * @param path Path to walk. */
export function matchDirSync (pats: Pattern[], path: string): Generator<Matched> {
  return _matchDirSync(pats, path, '.')
}

function * _matchDirSync (pats: Pattern[], root: string, path: string): Generator<Matched> {
  for (const entry of readdirSync(join(root, path), { withFileTypes: true })) {
    const { yay, nay } = _checkPatterns(pats, join(path, entry.name))
    if (yay.length > 0) {
      yield _getMatched(yay, root, path, entry)
    }
    if (nay.length > 0 && entry.isDirectory()) {
      yield * _matchDirSync(nay, root, join(path, entry.name))
    }
  }
}

/** Split an array of patterns into those that do and don't match a path. */
function _checkPatterns (pats: Pattern[], path: string): {
  yay: Pattern[]
  nay: Pattern[]
} {
  const yay: Pattern[] = []
  const nay: Pattern[] = []
  for (const pat of pats) {
    if (pat.matches(path)) {
      yay.push(pat)
    } else {
      nay.push(pat)
    }
  }
  return { yay, nay }
}

/** Return a `Matched` representing a given directory entry. */
function _getMatched (pats: Pattern[], root: string, path: string, entry: Dirent): Matched {
  if (entry.isDirectory()) {
    return {
      pats,
      name: entry.name,
      path,
      isDir: true,
      isLink: false,
      walk: () => walkDir(join(root, path, entry.name)),
      walkSync: () => walkDirSync(join(root, path, entry.name))
    }
  } else {
    return {
      pats,
      name: entry.name,
      path,
      isDir: false,
      isLink: entry.isSymbolicLink()
    }
  }
}

/** Copy the contents of a directory from one location to another, taking only
  * the paths that match the given set of match patterns, and then optionally
  * mapping them to a different directory within the destination. Any symlinks
  * matched will be copied via `copyLink`, files via `copyFile`, and
  * sub-directories via `copyDir`.
  * @details In the interest of avoiding side-effects when overlaying files
  *          from multiple sources, this function adheres to specific rules
  *          when copying to a destination directory that already exists.
  *          Attempting to copy any file or symlink that would overwrite an
  *          existing path in the destination directory will fail. Copying
  *          a sub-directory over an existing path will succeed, provided
  *          that the destination path is also a directory, in which case
  *          the contents of the two will be merged following the same rules.
  * @param pats Patterns to match.
  * @param src Source path.
  * @param dst Destination path. */
export async function copyMatches (pats: Pattern[], src: string, dst: string): Promise<void> {
  const dirs = new DirSet()
  for await (const entry of matchDir(pats, src)) {
    for (const pat of entry.pats) {
      const srcPath = join(src, entry.path, entry.name)
      const dstPath = join(dst, pat.mapping(join(entry.path, entry.name)))
      if (entry.isDir) {
        await copyDir(srcPath, dstPath)
      } else {
        await dirs.mk(dirname(dstPath))
        if (entry.isLink) {
          await copyLink(srcPath, dstPath)
        } else {
          await copyFile(srcPath, dstPath)
        }
      }
    }
  }
}

/** Copy the contents of a directory synchronously from one location to another,
  * taking only the paths that match the given set of match patterns, and then
  * optionally mapping them to a different directory within the destination.
  * Any symlinks matched will be copied via `copyLinkSync`, files via `copyFileSync`,
  * and sub-directories via `copyDirSync`.
  * @details In the interest of avoiding side-effects when overlaying files
  *          from multiple sources, this function adheres to specific rules
  *          when copying to a destination directory that already exists.
  *          Attempting to copy any file or symlink that would overwrite an
  *          existing path in the destination directory will fail. Copying
  *          a sub-directory over an existing path will succeed, provided
  *          that the destination path is also a directory, in which case
  *          the contents of the two will be merged following the same rules.
  * @param pats Patterns to match.
  * @param src Source path.
  * @param dst Destination path. */
export function copyMatchesSync (pats: Pattern[], src: string, dst: string): void {
  const dirs = new DirSet()
  for (const entry of matchDirSync(pats, src)) {
    for (const pat of entry.pats) {
      const srcPath = join(src, entry.path, entry.name)
      const dstPath = join(dst, pat.mapping(join(entry.path, entry.name)))
      if (entry.isDir) {
        copyDirSync(srcPath, dstPath)
      } else {
        dirs.mkSync(dirname(dstPath))
        if (entry.isLink) {
          copyLinkSync(srcPath, dstPath)
        } else {
          copyFileSync(srcPath, dstPath)
        }
      }
    }
  }
}

/** Companion to `copyMatches`; copy the contents of a directory from one location
  * to another, taking only the paths that match the given set of match patterns,
  * and then optionally mapping them to a different directory within the destination.
  * Creates hardlinks rather than actual copies for matched files or symlinks
  * via `linkFile`; matched directories are copied via `linkDir`.
  * @details In the interest of avoiding side-effects when overlaying files
  *          from multiple sources, this function adheres to specific rules
  *          when copying to a destination directory that already exists.
  *          Attempting to copy any file or symlink that would overwrite an
  *          existing path in the destination directory will fail. Copying
  *          a sub-directory over an existing path will succeed, provided
  *          that the destination path is also a directory, in which case
  *          the contents of the two will be merged following the same rules.
  * @param pats Patterns to match.
  * @param src Source path.
  * @param dst Destination path. */
export async function linkMatches (pats: Pattern[], src: string, dst: string): Promise<void> {
  const dirs = new DirSet()
  for await (const entry of matchDir(pats, src)) {
    for (const pat of entry.pats) {
      const srcPath = join(src, entry.path, entry.name)
      const dstPath = join(dst, pat.mapping(join(entry.path, entry.name)))
      if (entry.isDir) {
        await linkDir(srcPath, dstPath)
      } else {
        await dirs.mk(dirname(dstPath))
        await linkFile(srcPath, dstPath)
      }
    }
  }
}

/** Companion to `copyMatchesSync`; copy the contents of a directory from one location
  * to another, taking only the paths that match the given set of match patterns, and
  * then optionally mapping them to a different directory within the destination.
  * Creates hardlinks rather than actual copies for matched files or symlinks
  * via `linkFileSync`; matched directories are copied via `linkDirSync`.
  * @details In the interest of avoiding side-effects when overlaying files
  *          from multiple sources, this function adheres to specific rules
  *          when copying to a destination directory that already exists.
  *          Attempting to copy any file or symlink that would overwrite an
  *          existing path in the destination directory will fail. Copying
  *          a sub-directory over an existing path will succeed, provided
  *          that the destination path is also a directory, in which case
  *          the contents of the two will be merged following the same rules.
  * @param pats Patterns to match.
  * @param src Source path.
  * @param dst Destination path. */
export function linkMatchesSync (pats: Pattern[], src: string, dst: string): void {
  const dirs = new DirSet()
  for (const entry of matchDirSync(pats, src)) {
    for (const pat of entry.pats) {
      const srcPath = join(src, entry.path, entry.name)
      const dstPath = join(dst, pat.mapping(join(entry.path, entry.name)))
      if (entry.isDir) {
        linkDirSync(srcPath, dstPath)
      } else {
        dirs.mkSync(dirname(dstPath))
        linkFileSync(srcPath, dstPath)
      }
    }
  }
}

/** Used to stop multiple `mkdir` calls for the same
  * directory when copying files to the destination. */
class DirSet extends Set<string> {
  /** Create directory if not already created by this set. */
  async mk (path: string): Promise<void> {
    if (!this.has(path)) {
      await mkdir(path, { recursive: true })
      this.add(path)
    }
  }

  /** Synchronously create directory if not already created by this set. */
  mkSync (path: string): void {
    if (!this.has(path)) {
      mkdirSync(path, { recursive: true })
      this.add(path)
    }
  }
}
