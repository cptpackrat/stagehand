module.exports = {
  verbose: true,
  testEnvironment: 'node',
  testMatch: [
    '<rootDir>/test/**/*.ts'
  ],
  transform: {
    '.(ts|tsx)': require.resolve('ts-jest')
  }
}
